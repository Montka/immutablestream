package lab.ImmutableStream;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.function.*;
import java.util.stream.Collector;


public abstract class ImmutableStream<T extends Object> implements Iterator {

    protected Iterator parent;
    protected abstract T compute();
    private List<T> vault = new ArrayList<>();
    boolean terminated=false;
    private void terminate()
    {
        parent=vault.iterator();
        terminated=true;
    }
    public boolean hasNext() {
        return parent.hasNext();
    }

    public T next()
    {
        if(!terminated) {
            T tmp = compute();
            vault.add(tmp);
            return tmp;
        }
        else return (T) parent.next();
    }
    public ImmutableStream(Iterator src) {
        parent = src;
    }

    public static <R> ImmutableStream<R> build(Iterable<R> src)
    {
        return new ImmutableStream<R>(src.iterator()) {
            @Override
            protected R compute() {
                return (R) this.parent.next();
            }
        };
    }

    public static <R> ImmutableStream<R> generate (Supplier<R> sup)
    {
       class generatedImmutableStream<R> extends ImmutableStream {
           @Override
           protected R compute() {return (R) sup.get();}
           @Override
           public boolean hasNext(){return true;}

           public generatedImmutableStream() {super(null);}
       }
        return  new generatedImmutableStream<R>();
    }

    public <R> ImmutableStream<R> map(Function<? super T,? extends R> function)
    {
        return new ImmutableStream<R>(this)
        {
            @Override
            protected R compute() {
                return function.apply((T) parent.next());
            }
        };
    }

    public ImmutableStream<T> filter(Predicate<? super T> predicate)
    {
        return new ImmutableStream<T>(this)
        {
            @Override
            protected T compute() {
                while (hasNext())
                {
                    T tmp=(T) parent.next();
                    if (predicate.test(tmp)){return tmp;}
                }
                return null;
            }
        };
    }

    public ImmutableStream<T> peek(Consumer<T> func)
    {
        return new ImmutableStream<T>(this) {
            @Override
            protected T compute() {
                T element=(T) parent.next();
                func.accept(element);
                return element;
            }
        };
    }

    public ImmutableStream<T> limit(int lim)
    {
        return new ImmutableStream<T>(this) {
            int _current_count=0;
            @Override
            protected T compute() {
                {
                    if (parent.hasNext() && _current_count<lim)
                    {
                        ++_current_count;
                        return (T) parent.next();
                    }
                    //else {throw new NoSuchElementException();}
                    else return null;
                }
            }
            @Override
        public boolean hasNext(){
                return (_current_count<lim && parent.hasNext());
            }
        };
    }

    public ImmutableStream<T> skip(int count)
    {
        return new ImmutableStream<T>(this) {
            int _last=count;
            @Override
            protected T compute() {
                while(hasNext())
                {
                    if (_last>0)
                    {
                    parent.next();
                    --_last;}
                    else return (T) parent.next();
                }
                return null;
            }
        };
    }



    public boolean allMatch(Predicate<? super T> predicate)
    {
        boolean res=true;
        while (hasNext())
        {
            res &= predicate.test((T) parent.next());
        }
        terminate();
    return res;
    }

    public boolean anyMatch(Predicate<? super T> predicate)
    {
        boolean res=false;
        while (hasNext())
        {
            res |= predicate.test((T) parent.next());
        }
        terminate();
    return res;
    }

    public int count()
    {
        int i;
        for(i=0; hasNext(); ++i){}
        terminate();
        return i;
    }

    public <R,A> R collect(Collector<? super T,A,R> collector)
    {
        A result = collector.supplier().get();
        while (hasNext())
        collector.accumulator().accept(result, next());
        terminate();
        return collector.finisher().apply(result);
    }

    public T reduce(T identity, BinaryOperator<T> accumulator)
    {
        T result = identity;
        while (hasNext())
            result=accumulator.apply(result,next());
        terminate();
        return result;
    }

    public void forEach(Consumer<T> action)
    {
        while (hasNext())
            action.accept( next());
        terminate();
    }

    public  void finalize() {
        if(!terminated) {
            while (hasNext())
                next();
            terminate();
        }
    }
}
