package lab.ImmutableStream;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;


public class Main {

    public static void main(String[] args) {
        // write your code here
        List<String> a = Arrays.asList("aa", "BDDB","ffAD","X","XX");
        ImmutableStream<String> str = ImmutableStream.build(a);
        ImmutableStream<Integer> gen = ImmutableStream.generate(new Supplier<Integer>() {
            int a=1;
            int b=0;
            @Override
            public Integer get() {
                int tmp=a+b;
                b=a;
                a=tmp;

                return a;
            }
        });
        ImmutableStream<Integer> count = ImmutableStream.generate(new Supplier<Integer>() {
            Integer x = 0;

            @Override
            public Integer get() {
                return ++x;
            }
        }).peek(x -> System.out.println(x)).limit(5).skip(4);


        for(int i=0;  i<40; ++i)
        System.out.println(gen.next());
        System.out.println(count.reduce(0,(x,y)->x+y));

        //System.out.println(str.collect(Collectors.counting()));
        //for(int i=0;  i<20; ++i)
        //System.out.println(str.next());

    }
}
